# Weather App #

The Weather application has a minimalist UI that can show the weather forecast for the next five days in your region. It can also detect your location using your device’s GPS. 

## Objective: ##

This application is a personal pet project. The objective was to use an external API to fetch the weather data and present it to the user.

## Screenshots: ##

![](screenshots.svg)

### Project Dependencies:

This project uses the following **CocoaPods** libraries:
   -  **Alamofire 5.2.1**
   -  **SwiftyJSON 5.0.0**
   -  **NVActivityIndicatorView 4.8.0**



### About Me:

I am an iOS developer for more than 2 years and I am extremely motivated to expand my knowledge and embrace a new challenge.
Due to my early contact with the sales business, I was able to develop a solid sense of product which is key in the mobile development process. My main goal is to create innovative and solid applications.

## Contacts: ##

 - +33 (0)7 49 27 87 17
 - ios.tiagosantos@gmail.com
 - www.linkedin.com/in/tiagoandresantos
