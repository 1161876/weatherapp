//
//  WeatherAppUITests.swift
//  WeatherAppUITests
//
//  Created by Tiago  Santos on 24/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Weather

class WeatherAppUITests: XCTestCase {
	
	let app =  XCUIApplication()
	
	override func setUp() {
		super.setUp()
		continueAfterFailure = false
		app.launch()
	}
	
	func testShouldAppearDefaultWeather(){
		
		let app = XCUIApplication()
		app.buttons["Change City"].tap()
		
		//sleep to wait for weatherAPI response and update UI
		sleep(2)
	
		XCTAssertEqual(app.tables.cells.count, 5)
		let cityNameLabel = app.staticTexts["cityNameLabel"]
		XCTAssertEqual("Porto", cityNameLabel.label)
	}
	
	func testAlertInputShouldSelectAndDisplayLisbonWeather(){
		
		let app = XCUIApplication()
		app.buttons["Change City"].tap()
		
		let alertView = app.alerts["Select New City"].scrollViews.otherElements
		alertView.collectionViews/*@START_MENU_TOKEN@*/.textFields["City name"]/*[[".cells.textFields[\"City name\"]",".textFields[\"City name\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
		alertView.collectionViews/*@START_MENU_TOKEN@*/.textFields["City name"]/*[[".cells.textFields[\"City name\"]",".textFields[\"City name\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.typeText("Lisbon")
		alertView.buttons["OK"].tap()
		
		//sleep to wait for weatherAPI response and update UI
		sleep(2)
		
		XCTAssertEqual(app.tables.cells.count, 5)
		let cityNameLabel = app.staticTexts["cityNameLabel"]
		XCTAssertEqual("Lisbon", cityNameLabel.label)
	}
	
	
	func testGetLocationShouldSelectAndDisplayUnitedStatesWeather(){
		let app = XCUIApplication()
		
		//sleep to make sure that the default city is loaded
		sleep(2)

		app.buttons["locationButton"].tap()
		
		//sleep to wait for weatherAPI response and update UI
		sleep(2)
		
		XCTAssertEqual(app.tables.cells.count, 5)
	}
	
	func testFailToGetCity(){
		let app = XCUIApplication()
		app.buttons["Change City"].tap()
		
		let alertView = app.alerts["Select New City"].scrollViews.otherElements
		alertView.collectionViews/*@START_MENU_TOKEN@*/.textFields["City name"]/*[[".cells.textFields[\"City name\"]",".textFields[\"City name\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
		alertView.collectionViews/*@START_MENU_TOKEN@*/.textFields["City name"]/*[[".cells.textFields[\"City name\"]",".textFields[\"City name\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.typeText("InvalidCityName")
		alertView.buttons["OK"].tap()
		
		//sleep to wait for weatherAPI response and update UI
		sleep(2)
		
		XCTAssertEqual(app.alerts.element.label, "Ups, seems that:")
	}
}
