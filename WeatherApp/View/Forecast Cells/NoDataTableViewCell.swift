//
//  NoDataTableViewCell.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 25/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

class NoDataTableViewCell: UITableViewCell {
	
	@IBOutlet weak var errorDescriptionLabel: UILabel!
	
	func setErrorLabel(error: String){
		errorDescriptionLabel.text = error
	}
}
