//
//  ForecastTableViewCell.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 24/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {
	
	@IBOutlet weak var dayLabel: UILabel!
	@IBOutlet weak var weatherIconImage: UIImageView!
	@IBOutlet weak var maxTempLabel: UILabel!
	@IBOutlet weak var minTempLabel: UILabel!
	
	func setCell(dayForecast: Forecast){
		dayLabel.text = dayForecast.getDay()
		maxTempLabel.text  = dayForecast.getTemp().getMaxTemp()
		minTempLabel.text  = dayForecast.getTemp().getMinTemp()

		if let iconImage  = UIImage(named: dayForecast.getIcon()){
			weatherIconImage.image = iconImage
		}else{
			//load default image
			weatherIconImage.image = UIImage(named: "1")
		}
	}
}
