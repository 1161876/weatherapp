//
//  ForecastView.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 12/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ForecastView: UIView{
	@IBOutlet weak var changeCityButtonViewConstraint: NSLayoutConstraint!
	@IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
	
	@IBOutlet weak var forecastTableView: UITableView!
	@IBOutlet weak var cityNameLabel: UILabel!
	@IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
	@IBOutlet weak var changeCityButton: UIButton!
	@IBOutlet weak var locationButton: UIButton!
	let refreshControl = UIRefreshControl()
	
	func registerCustomTableViewCells(){
		forecastTableView.register(UINib.init(nibName: "ForecastTableViewCell", bundle: nil), forCellReuseIdentifier: "ForecastCell")
		forecastTableView.register(UINib.init(nibName: "NoDataTableViewCell", bundle: nil), forCellReuseIdentifier: "NoDataCell")
	}
	
	func setUpViewsConstraints(viewHeight: CGFloat){
		changeCityButtonViewConstraint.constant = viewHeight*0.15
		topViewHeightConstraint.constant = viewHeight*0.2
	}
	
	func setUpRefreshView(){
		refreshControl.attributedTitle = NSAttributedString(string: "Pull to Retry")
		forecastTableView.addSubview(refreshControl)
	}
	
}
