//
//  ForecastTableViewController.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 24/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

extension ForecastViewController: UITableViewDelegate, UITableViewDataSource{
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		//Show forecast information
		if weatherForecast != nil{
			return weatherForecast!.count
		}
		
		//Wait to fetch data
		if error == nil{
			return 0
		}
		
		//Load "no data" cell
		self.forecastView.cityNameLabel.text = "Forecast"
		return 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		forecastView.loadingIndicator.stopAnimating()
		forecastView.refreshControl.endRefreshing()
		
		//Return no data cell
		if weatherForecast == nil{
			let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell") as! NoDataTableViewCell
			if let error = self.error{
				cell.setErrorLabel(error: error)
			}
			cell.selectionStyle = .none
			self.forecastView.loadingIndicator.stopAnimating()
			return cell
		}
		
		//Return forecast cell
		self.forecastView.cityNameLabel.text = dataLoader!.getCurrentCity()
		let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell") as! ForecastTableViewCell
		cell.setCell(dayForecast: self.weatherForecast![indexPath.row])
		cell.selectionStyle = .none
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return (view.frame.height*0.6)/5
	}
	
}
