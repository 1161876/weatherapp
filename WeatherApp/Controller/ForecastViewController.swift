//
//  ViewController.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 24/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import UIKit

class ForecastViewController: UIViewController, ForecastSubscriber{
	
	@IBOutlet var forecastView: ForecastView!
	var dataLoader:DataLoader?
	var weatherForecast:[Forecast]? = nil
	var error: String? = nil

	override func viewDidLoad() {
		super.viewDidLoad()
		dataLoader = DataLoader(forecastVC: self)
		dataLoader!.getForecast()
		forecastView.setUpRefreshView()
		forecastView.loadingIndicator.startAnimating()
		forecastView.registerCustomTableViewCells()
		setButtonsTarget()
	}
	
	override func viewWillLayoutSubviews() {
		forecastView.setUpViewsConstraints(viewHeight: view.frame.height)
	}
	
	private func setButtonsTarget(){
		forecastView.changeCityButton.addTarget(self, action: #selector(changeCityPressed), for: .touchUpInside)
		forecastView.locationButton.addTarget(self, action: #selector(currentLocationPressed), for: .touchUpInside)
		forecastView.refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
	}
	
	@objc func reloadData(_ sender: Any) {
		self.forecastView.loadingIndicator.startAnimating()
		dataLoader!.getForecast()
	}
	
	@objc func changeCityPressed(_ sender: UIButton) {
		
		let alert = UIAlertController(title: "Select New City", message: "Please enter the city name", preferredStyle: .alert)

		alert.addTextField { (textField) in
			textField.placeholder = "City name"
			textField.keyboardType = .alphabet
		}

		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
			let textField = alert!.textFields![0]
			if textField.text! != ""{
				self.forecastView.loadingIndicator.startAnimating()
				self.dataLoader!.getCityByString(name: textField.text!)
			}
		}))

		self.present(alert, animated: true, completion: nil)
	}
	
	@objc func currentLocationPressed(_ sender: UIButton) {
		self.forecastView.loadingIndicator.startAnimating()
		self.dataLoader!.getForecastForCurrentLocation()
	}

	func presentAlert(alertMessage: String){
		let alert = UIAlertController(title: "Ups, seems that:", message: alertMessage, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		self.present(alert, animated: true, completion: nil)
	}
	
	func didReceiveErrorMessage(error: String){
		forecastView.loadingIndicator.stopAnimating()
		presentAlert(alertMessage: error)
		self.error = error
	}
	
	func didReceiveForecast(forecast: [Forecast]){
		forecastView.loadingIndicator.stopAnimating()
		weatherForecast = forecast
		error = nil
		self.forecastView.forecastTableView.reloadData()
	}
}
