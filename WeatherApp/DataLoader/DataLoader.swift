//
//  DataLoader.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 20/05/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

class DataLoader{
	
	var controller: ForecastSubscriber?
	var location = Location()
	private var currentCity: City?
	private var newCityName: String?
	
	init(forecastVC: ForecastSubscriber){
		self.controller = forecastVC
		self.currentCity = try? City(name: "Porto", key: "275317")
		location.locationDelegate = self
	}
	
	func getForecast(){
		
		if currentCity == nil{
			return
		}

		if let key = currentCity!.getKey(){
			WeatherAPI.getForecast(cityKey: key){ (result) in
				switch result {
				case .success(let forecast):
					self.controller?.didReceiveForecast(forecast: forecast)
				case .failure (let error):
					if !self.isNetworkConnectionError(error: error.localizedDescription){
						self.controller?.didReceiveErrorMessage(error: "Failed to get city forecast")
					}
				}
			}
		}
	}
	
	func getForecastForCurrentLocation(){
		location.getCurrentLocation()
	}
	
	private func getCityKeyByLocation(latitude: String, longitude: String){
		WeatherAPI.getCityKeyByLocation(latitude: latitude, longitude: longitude) { (result) in
			switch result {
			case .success(let city):
				self.currentCity = city
				self.getForecast()
			case .failure(let error):
				if !self.isNetworkConnectionError(error: error.localizedDescription){
					self.controller?.didReceiveErrorMessage(error: "Failed to get valid city from location")
				}
			}
		}
	}
	
	func getCityByString(name: String){
		do{
			let newCityName = try City.validateName(name: name)
			WeatherAPI.getCityByString(city: newCityName) { (result) in
				switch result{
				case .success(let city):
					self.currentCity = city
					self.getForecast()
				case .failure(let error):
					if !self.isNetworkConnectionError(error: error.localizedDescription){
						self.controller?.didReceiveErrorMessage(error: "No forecast found for this city")
					}
				}
			}
		}catch{
			self.controller?.didReceiveErrorMessage(error: "Invalid city name")
		}
	}
	
	func getCurrentCity() -> String{
		if let city = self.currentCity{
			return city.getName()
		}else{
			return "No city selected"
		}
	}
	
	private func isNetworkConnectionError(error: String) -> Bool{
		let noInternetError = "URLSessionTask failed with error: The Internet connection appears to be offline."
		if error == noInternetError{
			self.controller?.didReceiveErrorMessage(error: "No internet connection")
			return true
		}
		return false
	}
}

extension DataLoader: LocationDelegate{
	func didUpdateCurrentLocation(latitude: String, longitude: String) {
		getCityKeyByLocation(latitude: latitude, longitude: longitude)
	}
}
