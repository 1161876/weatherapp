//
//  ForecastSubscriber.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 20/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

protocol ForecastSubscriber{
	func didReceiveErrorMessage(error: String)
	func didReceiveForecast(forecast: [Forecast])
}
