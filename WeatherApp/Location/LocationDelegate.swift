//
//  LocationDelegate.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 25/05/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

protocol LocationDelegate{
	func didUpdateCurrentLocation(latitude: String, longitude: String)
}
