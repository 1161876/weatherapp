//
//  Location.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 25/05/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import CoreLocation

class Location: NSObject, CLLocationManagerDelegate{
	
	var locationDelegate: LocationDelegate?
	private let locationManager = CLLocationManager()
	private var haveCurrentLocation = false
	
	func getCurrentLocation(){
		haveCurrentLocation = false
		locationManager.requestWhenInUseAuthorization()
		if CLLocationManager.locationServicesEnabled() {
			locationManager.delegate = self
			locationManager.desiredAccuracy = kCLLocationAccuracyBest
			locationManager.startUpdatingLocation()
		}
	}
	
	internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		if !haveCurrentLocation{
			haveCurrentLocation = true
			let location = locations[0]
			let latitude = String(location.coordinate.latitude)
			let longitude = String(location.coordinate.longitude)
			self.locationDelegate?.didUpdateCurrentLocation(latitude: latitude, longitude: longitude)
		}
	}
}
