//
//  File.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 25/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import SwiftyJSON
import Alamofire

enum WeatherAPIError: Error{
	case invalidForecasts
}

class WeatherAPI{
	
	private static let baseURL = "http://dataservice.accuweather.com"
	private static let privateKey = "PN1BEqAB5teVviB1d2acucFOYsjNLRIZ"
	//private let privateKey = "soLWjd76OGJJmKMzy9AJlWBc6M0yBAG5"
	//private let privateKey = "if74FzyGUjw68G1OIMZX8G3taomI8bKB"
	
	static func httpClientCall(url: String, completion: @escaping (Result<JSON,Error>) -> Void ){
		AF.request(url).responseJSON{
			response in
			switch response.result{
			case .success(let data):
				let json = JSON(data)
				completion(.success(json))
			case .failure(let error):
				completion(.failure(error))
			}
		}
	}
	
	static func getForecast(cityKey: String, completion: @escaping (Result<[Forecast],Error>) -> Void) {
		
		let url = "\(baseURL)/forecasts/v1/daily/5day/\(cityKey)?apikey=\(privateKey)&metric=true"
		
		httpClientCall(url: url) { (response) in
			switch response{
			case .success(let json):
				
				let forecastJSON = json["DailyForecasts"]
				
				if forecastJSON.count != 5{
					completion(.failure(WeatherAPIError.invalidForecasts))
				}else{
					var forecasts: [Forecast] = []
					for n in 0...forecastJSON.count - 1 {
						let day  = forecastJSON[n]["Date"].stringValue
						let maxTemp = forecastJSON[n]["Temperature"]["Maximum"]["Value"].stringValue
						let minTemp = forecastJSON[n]["Temperature"]["Minimum"]["Value"].stringValue
						let icon = forecastJSON[n]["Day"]["Icon"].stringValue
						
						do{
							let forecast  = try Forecast(day: day, maxTemp: maxTemp, minTemp: minTemp, icon: icon)
							forecasts.append(forecast)
						}catch let error{
							print(error)
						}
					}
					if forecasts.count == 5{
						completion(.success(forecasts))
					}else{
						completion(.failure(WeatherAPIError.invalidForecasts))
					}
				}
			case .failure(let error):
				completion(.failure(error))
			}
		}
	}
	
	static func getCityKeyByLocation(latitude: String, longitude: String, completion: @escaping (Result<City,Error>) -> Void) {
		
		let cityCoordinates = latitude + "%2C" + longitude
		let url = "\(baseURL)/locations/v1/cities/geoposition/search?apikey=\(privateKey)&q=\(cityCoordinates)"
		
		httpClientCall(url: url) { (response) in
			switch response{
			case .success(let json):
				
				let cityName = json["Country"]["LocalizedName"].stringValue
				let cityKey = json["Key"].stringValue
				do {
					let city = try City(name: cityName, key: cityKey)
					completion(.success(city))
				} catch let error {
					completion (.failure(error))
				}
			case .failure(let error):
				completion(.failure(error))
			}
		}
	}
	
	static func getCityByString(city: String, completion: @escaping (Result<City,Error>) -> Void){

		let cityString = city.replacingOccurrences(of: " ", with: "+", options: .literal, range: nil)
		let url = "\(baseURL)/locations/v1/cities/search?apikey=\(privateKey)&q=\(cityString)&details=false"
		
		httpClientCall(url: url) { (response) in
			switch response{
			case .success(let json):
				let cities = json[]
				let cityName = cities[0]["LocalizedName"].stringValue
				let cityKey = cities[0]["Key"].stringValue
				
				do{
					let city = try City(name: cityName, key: cityKey)
					completion(.success(city))
				}catch let error{
					completion(.failure(error))
				}

			case .failure(let error):
				completion(.failure(error))
			}
		}
	}
	
}
