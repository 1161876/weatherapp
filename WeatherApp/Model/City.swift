//
//  City.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 25/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

enum CityError: Error{
	case invalidName
	case invalidKey
}

class City{
	
	private var name: String
	private var key: String?
	
	init(name: String, key: String) throws {
		let name = try City.validateName(name: name)
		let key = try City.validateKey(key: key)
		self.name = name
		self.key = key
	}
	
	func getName()->String{
		return name
	}
	
	func getKey()->String?{
		return key
	}
	
	class func validateName(name: String) throws -> String {
		if name != "" && !name.containsNumbers(){
			return name.capitalizingFirstLetter()
		}
		throw CityError.invalidName
	}
	
	class func validateKey(key: String) throws -> String {
		if key != "" && !key.containsWhiteSpace(){
			if let _ = Int(key){
				return key
			}
		}
		throw CityError.invalidKey
	}
}
