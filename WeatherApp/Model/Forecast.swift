//
//  Forecast.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 26/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import Foundation

enum ForecastError: Error{
	case invalidDay
}

class Forecast{
	
	private var day: Date
	private var temp: Temperature
	private var icon: String
	
	init(day: String, maxTemp: String, minTemp: String, icon: String) throws {
		let day = try Forecast.validateDay(day: day)
		let temp = try Forecast.validateTemp(minTemp: minTemp, maxTemp: maxTemp)
		self.icon = icon
		self.day = day
		self.temp = temp
		self.icon = icon
	}
	
	class func validateDay(day: String) throws -> Date{
		if let date = Date.dateFromISOString(string: day){
			return date
		}
		throw ForecastError.invalidDay
	}
	
	class func validateTemp(minTemp: String, maxTemp: String) throws -> Temperature{
		let temp = try Temperature(maxTemp: maxTemp, minTemp: minTemp)
		return temp
	}
	
	func getDay() -> String{
		return self.day.getDayOfWeek()
	}
	
	func getTemp() -> Temperature{
		return self.temp
	}
	
	func getIcon() -> String{
		return icon
	}
	
}
