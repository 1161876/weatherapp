//
//  Date.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 24/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import Foundation

extension Date {
	
	static func dateFromISOString(string: String) -> Date? {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
		return dateFormatter.date(from: string)
	}
	
	func getDayOfWeek() -> String{
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "EE"
		return dateFormatter.string(from: self).capitalized
	}

}
