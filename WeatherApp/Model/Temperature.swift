//
//  Temperature.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 26/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//
enum TemperatureError: Error{
	case invalidTempValues
}

class Temperature{
	private var maxTemp: Double
	private var minTemp: Double
	
	init(maxTemp: String, minTemp: String) throws {
		let maxTemp = try Temperature.validateTempValue(value: maxTemp)
		let minTemp = try Temperature.validateTempValue(value: minTemp)
		self.maxTemp = maxTemp
		self.minTemp = minTemp
	}
	
	class func validateTempValue(value: String) throws -> Double{
		if let value = Double(value){
			if (value > -60 && value < 60){
				return value.rounded(toPlaces: 1)
			}
		}
		throw TemperatureError.invalidTempValues
	}
	
	func getMaxTemp()-> String{
		return String(maxTemp) + "ºC"
	}
	
	func getMinTemp()-> String{
		return String(minTemp) + "ºC"
	}
}
