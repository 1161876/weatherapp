//
//  String.swift
//  WeatherApp
//
//  Created by Tiago  Santos on 25/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import Foundation

extension String{
	
	func capitalizingFirstLetter() -> String {
		return prefix(1).capitalized + dropFirst()
	}
	
	func containsNumbers() -> Bool {
		
		let decimalCharacters = CharacterSet.decimalDigits
		let invalidCharacters = self.rangeOfCharacter(from: decimalCharacters)

		if let _ = invalidCharacters{
			return true
		}
		return false
	}
	
	func containsWhiteSpace() -> Bool {
		let decimalCharacters = CharacterSet.whitespaces

		let invalidCharacters = self.rangeOfCharacter(from: decimalCharacters)

		if let _ = invalidCharacters{
			return true
		}
		
		return false
	}
}
