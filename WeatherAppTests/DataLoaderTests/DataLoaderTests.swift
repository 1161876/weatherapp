//
//  DataLoaderTests.swift
//  WeatherAppTests
//
//  Created by Tiago  Santos on 25/05/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Weather

class DataLoaderTests: XCTestCase{
	
	let forecastSubs = MockForecastSubscriber()

	func testGetForecastForDefaultCity() {
		let expect = expectation(description: "Test should return forecast for Porto City")
		forecastSubs.asyncExpectation = expect

		forecastSubs.dataLoader!.getForecast()

		waitForExpectations(timeout: 2) { error in
			if let error = error {
				XCTFail("waitForExpectationsWithTimeout error: \(error)")
			}
			XCTAssertEqual(self.forecastSubs.forecast?.count, 5)
			XCTAssertEqual(self.forecastSubs.dataLoader!.getCurrentCity(), "Porto")
			XCTAssertNil(self.forecastSubs.error)
			self.forecastSubs.asyncExpectation = nil
		}
	}
	
	
	
	func testGetCityByName(){
		let expect = expectation(description: "Should get city with a valid key and name")
		forecastSubs.asyncExpectation = expect
		
		forecastSubs.dataLoader!.getCityByString(name: "Paris")
		
		waitForExpectations(timeout: 2) { error in
			if let error = error {
				XCTFail("waitForExpectationsWithTimeout error: \(error)")
			}
			XCTAssertEqual(self.forecastSubs.forecast?.count, 5)
			XCTAssertEqual(self.forecastSubs.dataLoader!.getCurrentCity(), "Paris")
			XCTAssertNil(self.forecastSubs.error)
			self.forecastSubs.asyncExpectation = nil
		}
	}
	
	func testFailToGetCityByName(){
		
		let expect = expectation(description: "Should fail to get city")
		
		forecastSubs.asyncExpectation = expect
		forecastSubs.dataLoader!.getCityByString(name: "InvalidCityName")
		
		waitForExpectations(timeout: 2) { error in
			if let error = error {
				XCTFail("waitForExpectationsWithTimeout error: \(error)")
			}
			XCTAssertNil(self.forecastSubs.forecast)
			XCTAssertEqual(self.forecastSubs.error,"No forecast found for this city")
			self.forecastSubs.asyncExpectation = nil
		}
	}
	
	func testGetCityByLocation(){
		let expect = expectation(description: "Should get city with a valid key and name")
		forecastSubs.asyncExpectation = expect
		
		forecastSubs.dataLoader!.getForecastForCurrentLocation()
		
		waitForExpectations(timeout: 2) { error in
			if let error = error {
				XCTFail("waitForExpectationsWithTimeout error: \(error)")
			}
			XCTAssertEqual(self.forecastSubs.forecast?.count, 5)
			XCTAssertNil(self.forecastSubs.error)
			self.forecastSubs.asyncExpectation = nil
		}
	}
}
