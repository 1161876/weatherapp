//
//  SpyDataLoaderDelegate.swift
//  WeatherAppTests
//
//  Created by Tiago  Santos on 25/05/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import Foundation
import XCTest
@testable import Weather

class MockForecastSubscriber: ForecastSubscriber{
	
	var asyncExpectation: XCTestExpectation?
	var dataLoader: DataLoader?
	var forecast: [Forecast]? = nil
	var error: String? = nil
	
	init() {
		dataLoader = DataLoader(forecastVC: self)
	}
	
	func didReceiveErrorMessage(error: String) {
		guard let errorExpectation = asyncExpectation else {
			return
		}
		self.error = error
		errorExpectation.fulfill()
	}
	
	func didReceiveForecast(forecast: [Forecast]) {
		guard let forecastExpectation = asyncExpectation else {
			return
		}
		
		self.forecast = forecast
		forecastExpectation.fulfill()
	}
	
}
