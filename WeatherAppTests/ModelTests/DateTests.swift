//
//  DateTests.swift
//  WeatherAppTests
//
//  Created by Tiago  Santos on 26/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Weather

class DateTests: XCTestCase {
	
	func testConvertStringIntoDateSuccess(){
		let date = Date.dateFromISOString(string: "2020-04-17T07:00:00+01:00")
		if date == nil{
			XCTFail()
		}else{
			let calendarDate = Calendar.current.dateComponents([.day, .year, .month], from: date!)
			XCTAssertEqual(calendarDate.year!, 2020)
			XCTAssertEqual(calendarDate.month!, 04)
			XCTAssertEqual(calendarDate.day!, 17)
		}
	}
	
	func testConvertStringIntoDateFail(){
		let date = Date.dateFromISOString(string: "invalid string")
		XCTAssertNil(date)
	}

	func testGetDayOfTheWeek(){
		let date = Date.dateFromISOString(string: "2020-04-17T07:00:00+01:00")
		XCTAssertEqual(date!.getDayOfWeek(), "Fri")
	}
}
