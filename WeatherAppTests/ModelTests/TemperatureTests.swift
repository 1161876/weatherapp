//
//  TemperatureTests.swift
//  WeatherAppTests
//
//  Created by Tiago  Santos on 26/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Weather

class TemperatureTests: XCTestCase {
	
	func testCorrectTemperature(){
		do{
			let temp = try Temperature(maxTemp: "10.0", minTemp: "-10.0")
			XCTAssertEqual(temp.getMaxTemp(), "10.0ºC")
			XCTAssertEqual(temp.getMinTemp(), "-10.0ºC")
		}catch{
			XCTFail()
		}
	}

	func testIncorrectMinTempAndCorrectMaxTemp(){
		XCTAssertThrowsError(try Temperature(maxTemp: "10.0", minTemp: ""))
		XCTAssertThrowsError(try Temperature(maxTemp: "10.0", minTemp: "minTemp"))
		XCTAssertThrowsError(try Temperature(maxTemp: "10.0", minTemp: "-10 0"))
	}
	
	func testIncorrectMaxTempAndCorrectMinTemp(){
		XCTAssertThrowsError(try Temperature(maxTemp: "", minTemp: "10.0"))
		XCTAssertThrowsError(try Temperature(maxTemp: "maxTemp", minTemp: "10.0"))
		XCTAssertThrowsError(try Temperature(maxTemp: "10. 0", minTemp: "10.0"))
	}
}
