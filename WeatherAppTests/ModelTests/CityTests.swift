//
//  CityTests.swift
//  WeatherAppTests
//
//  Created by Tiago  Santos on 26/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Weather

class CityTests: XCTestCase {
	
	func testValidCity_InsertedValidNameAndKey(){
		do{
			let city = try City(name: "porto", key: "12345")
			XCTAssertEqual(city.getName(), "Porto")
			XCTAssertEqual(city.getKey(), "12345")
		}catch let error{
			XCTFail(error.localizedDescription)
		}
	}
	
	func testInvalidCityInserted_InvalidNameAndValidKey(){
		XCTAssertThrowsError(try City(name: "", key: "12345"))
		XCTAssertThrowsError(try City(name: "Porto1", key: "12345"))
	}
	
	func testInvalidCityInserted_ValidNameAndInvalidKey(){
		XCTAssertThrowsError(try City(name: "Porto", key: ""))
		XCTAssertThrowsError(try City(name: "Porto", key: "key"))
		XCTAssertThrowsError(try City(name: "Porto", key: "12 3456"))
	}
}
