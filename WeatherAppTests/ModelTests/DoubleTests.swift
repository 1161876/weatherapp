//
//  DoubleTests.swift
//  WeatherAppTests
//
//  Created by Tiago  Santos on 26/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Weather

class DoubleTests: XCTestCase {
	
	func testDoubleRoundedCorrectly(){
		let number = 1.1234567
		XCTAssertEqual(number.rounded(toPlaces: 2), 1.12)
	}
	
}
