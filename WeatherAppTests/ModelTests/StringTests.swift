//
//  StringTests.swift
//  WeatherAppTests
//
//  Created by Tiago  Santos on 26/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Weather

class StringTests: XCTestCase {

	func testCapitalizeFirstLetter(){
		let text = "porto"
		XCTAssertEqual(text.capitalizingFirstLetter(), "Porto")
	}
	
	func testStringHasSpaces(){
		let text = "Port o"
		XCTAssertTrue(text.containsWhiteSpace())
	}
	
	func testStringHasNoSpaces(){
		let text = "Porto"
		XCTAssertFalse(text.containsWhiteSpace())
	}
	
	func testStringHasNumbers(){
		let text = "Porto1"
		XCTAssertTrue(text.containsNumbers())
	}
	
	func testStringHasNoNumbers(){
		let text = "Porto"
		XCTAssertFalse(text.containsNumbers())
	}
}
