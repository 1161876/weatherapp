//
//  ForecastTests.swift
//  WeatherAppTests
//
//  Created by Tiago  Santos on 26/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Weather

class ForecastTests: XCTestCase {
	
	func testValidForecast(){
		XCTAssertNoThrow(try Forecast(day: "2020-04-17T07:00:00+01:00", maxTemp: "10.0", minTemp: "-10.0", icon: "1"))
	}
	
	func testInvalidForecastDay(){
		XCTAssertThrowsError(try Forecast(day: "InvalidDay", maxTemp: "10.0", minTemp: "-10.0", icon: "1"))
	}
	
	func testForecastGets(){
		do{
			let forecast = try Forecast(day: "2020-04-17T07:00:00+01:00", maxTemp: "10.0", minTemp: "-10.0", icon: "1")
			XCTAssertEqual(forecast.getDay(), "Fri")
			XCTAssertEqual(forecast.getTemp().getMaxTemp(), "10.0ºC")
			XCTAssertEqual(forecast.getTemp().getMinTemp(), "-10.0ºC")
			XCTAssertEqual(forecast.getIcon(), "1")
		}catch{
			XCTFail()
		}
	}
	
}
