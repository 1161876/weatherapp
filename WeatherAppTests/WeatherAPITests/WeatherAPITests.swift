//
//  WeatherAPITests.swift
//  WeatherAppTests
//
//  Created by Tiago  Santos on 26/04/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Weather

class WeatherAPITests: XCTestCase {
	
	func testFailToGetCityByLocation(){
		
		let expect = expectation(description: "Should fail to get city")
		
		WeatherAPI.getCityKeyByLocation(latitude: "360", longitude: "360"){ (result) in
			switch result {
			case .success( _):
				XCTFail()
			case .failure( _):
				expect.fulfill()
			}
		}
		
		waitForExpectations(timeout: 2)
	}
	
}
