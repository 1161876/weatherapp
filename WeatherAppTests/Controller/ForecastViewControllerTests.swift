//
//  ForecastViewControllerTests.swift
//  WeatherAppTests
//
//  Created by Tiago  Santos on 26/05/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Weather

class ForecastViewControllerTests: XCTestCase {

	let forecastVC = UIStoryboard(name: "Main", bundle: nil)
	.instantiateViewController(withIdentifier: "forecastViewController") as! ForecastViewController
	
	override func setUp() {
		super.setUp()
		forecastVC.beginAppearanceTransition(true, animated: false)
		forecastVC.endAppearanceTransition()
	}
	
	func testLoadingIndicatorIsAnimatingWhenLoadingData(){
		XCTAssertEqual(forecastVC.forecastView.loadingIndicator.isAnimating, true)
	}
	
	func testLoadingIndicatorIsNotAnimatingWhenFinishedLoadingData(){
		XCTAssertEqual(forecastVC.forecastView.loadingIndicator.isAnimating, true)
		forecastVC.didReceiveForecast(forecast: [])
		XCTAssertEqual(forecastVC.forecastView.loadingIndicator.isAnimating, false)
	}
	
	func testForecastListDisplayed(){
		//set up test variables
		let date = "2020-04-17T07:00:00+01:00"
		let maxTemp = "10.0"
		let minTemp = "5.0"
		let icon = "1"
		var forecastList: [Forecast] = []
		
		//create forecast for 5 days
		for _ in 1...5{
			forecastList.append(try! Forecast(day: date, maxTemp: maxTemp, minTemp: minTemp, icon: icon))
		}
		
		XCTAssertEqual(forecastList.count, 5)
		forecastVC.didReceiveForecast(forecast: forecastList)
		let numberOfDisplayedCells = forecastVC.forecastView.forecastTableView.numberOfRows(inSection: 0)
		XCTAssertEqual(numberOfDisplayedCells, 5)
	}

}
